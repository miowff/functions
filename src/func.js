const getSum = (str1, str2) => {
    if (str1.length === 0 && str2.length === 0) {
        return false;
    }
    if (/[a-zA-Z]/.test(str1)) {
        return false;
    }
    if (/[a-zA-Z]/.test(str2)) {
        return false;
    }   
    if (str1.length > str2.length) {

        let temp = str1
        str1 = str2
        str2 = temp
    }
    let result = "";
    let difference = str2.length - str1.length;
    let carry = 0;
    for (let i = str1.length - 1; i >= 0; i--) {
        let sum = ((str1.charCodeAt(i) - 48) + (str2.charCodeAt(i + difference) - 48) + carry);
        result += (sum % 10);
        carry = Math.floor(sum / 10);
    }
    for (let i = str2.length - str1.length - 1; i >= 0; i--) {
        let sum = ((str2.charCodeAt(i) - 48) + carry);
        result += sum % 10;
        carry = Math.floor(sum / 10);
    }
    if (carry) {
        result += (carry + '0');
    }
    result = result.split("").reverse().join("");
    return result;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let commentsCount = 0;
    let postsCount = 0;
    listOfPosts.forEach(post => {
        if (post.author === authorName) {
            postsCount++;
        }
        if (post.comments) {
            if (post.comments.length > 0) {
                post.comments.forEach(comment => {
                    if (comment.author === authorName) {
                        commentsCount++;
                    }
                })
            }
        }
    });
    return `Post:${postsCount},comments:${commentsCount}`;
};

const tickets = (people) => {
    var twentyFiveBill = 0;
    var fiftyBill = 0;
    for (var i = 0; i < people.length; i++) {
        if (people[i] === 25) {
            twentyFiveBill++;
        }
        if (people[i] === 50) {
            twentyFiveBill--;
            fiftyBill++;
        }
        if (people[i] === 100) {
            if (fiftyBill === 0 && twentyFiveBill >= 3) {
                twentyFiveBill -= 3;
            }
            else {
                twentyFiveBill--;
                fiftyBill--;
            }
        }
        if (twentyFiveBill < 0 || fiftyBill < 0) {
            return "NO";
        }
        return "YES";
    }
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
